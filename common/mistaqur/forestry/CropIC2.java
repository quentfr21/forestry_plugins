package mistaqur.forestry;

import java.util.ArrayList;
import java.util.List;

import forestry.api.cultivation.ICropEntity;

import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Block;
import net.minecraft.src.EntityItem;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.TileEntity;

import net.minecraft.src.World;
import ic2.api.TECrop;
import ic2.api.CropCard;


public class CropIC2 implements ICropEntity {

	private World world;
	private int xCoord;
	private int yCoord;
	private int zCoord;
	private TECrop te;

	public CropIC2(World world, int i, int j, int k) {
		this.world = world;
		this.xCoord = i;
		this.yCoord = j;
		this.zCoord = k;
		this.te = null;

		TileEntity entity = world.getBlockTileEntity(xCoord, yCoord, zCoord);
		if (entity != null) {
			if (entity instanceof TECrop) {
				this.te = (TECrop) entity;
			} else {
				ModLoader.getLogger().warning("TileEntity at position (" + xCoord + " " + yCoord + " " + zCoord+") is not instance of TECrop");
			}
		}
	}

	@Override
	public boolean isHarvestable() {
		if (this.te != null && te.id != -1) {
			CropCard cc = CropCard.getCrop(te.id);
			return cc.canBeHarvested(te) && !cc.canGrow(te);
		}
		return false;
	}

	@Override
	public int[] getNextPosition() {
		return null;
	}

	@Override
	public ArrayList<ItemStack> doHarvest() {
		ArrayList<ItemStack> harvest = new ArrayList<ItemStack>();
		if (this.te != null) {
			if (this.te.harvest(false)) {
				AxisAlignedBB axis = AxisAlignedBB.getBoundingBox(xCoord - 1.5, yCoord - 1.5, zCoord - 1.5,xCoord + 1.5, yCoord + 1.5, zCoord+1.5);
				List result = world.getEntitiesWithinAABB(EntityItem.class, axis);
				for (int ii = 0; ii < result.size(); ii++) {
					EntityItem entity = (EntityItem) result.get(ii);
					if (entity.isDead)
						continue;
					if (entity.item.stackSize <= 0)
						continue;
					harvest.add(entity.item);
					entity.setDead();
				}
			}
		}
		return harvest;
	}

}
