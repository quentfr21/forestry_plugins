package forestry.plugins;

import java.util.Random;

import forestry.api.core.IOreDictionaryHandler;
import forestry.api.core.IPacketHandler;
import forestry.api.core.IPickupHandler;
import forestry.api.core.IPlugin;
import forestry.api.core.IResupplyHandler;
import forestry.api.core.ISaveEventHandler;
import forestry.api.cultivation.CropProviders;

import net.minecraft.src.Block;
import net.minecraft.src.ICommand;
import net.minecraft.src.ItemStack;
import net.minecraft.src.ModLoader;
import net.minecraft.src.World;

import ic2.api.Items;

import cpw.mods.fml.common.network.IGuiHandler;

import mistaqur.forestry.CropProviderIC2;

public class PluginIC2Crops implements IPlugin {

	public static PluginIC2Crops instance;

	// IC2 stuff
	public static ItemStack crop;
	public static ItemStack cropSeed;

	public PluginIC2Crops() {
		if (PluginIC2Crops.instance == null)
			PluginIC2Crops.instance = this;
	}

	@Override
	public boolean isAvailable() {
		return ModLoader.isModLoaded("IC2");
	}

	@Override
	public void preInit() {
	}

	@Override
	public void doInit() {
		crop = Items.getItem("crop");
		cropSeed = Items.getItem("cropSeed");
		registerCropProviders();

	}

	@Override
	public void postInit() {
	}

	@Override
	public String getDescription() {
		return "IndustrialCraft2 Crops";
	}

	@Override
	public void generateSurface(World world, Random rand, int chunkX, int chunkZ) {
	};

	@Override
	public IGuiHandler getGuiHandler() {
		return null;
	}

	private void registerCropProviders() {
		CropProviders.cerealCrops.add(new CropProviderIC2());
	}

	@Override
	public IPacketHandler getPacketHandler() {
		return null;
	}

	@Override
	public IPickupHandler getPickupHandler() {
		return null;
	}

	@Override
	public IResupplyHandler getResupplyHandler() {
		return null;
	}

	@Override
	public ISaveEventHandler getSaveEventHandler() {
		return null;
	}

	@Override public IOreDictionaryHandler getDictionaryHandler() { return null; }
	@Override public ICommand[] getConsoleCommands() { return null; }


}
